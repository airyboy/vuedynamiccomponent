import Vue from 'vue'
import Parent from './Parent.vue'

new Vue({
  el: '#app',
  render: h => h(Parent)
})
